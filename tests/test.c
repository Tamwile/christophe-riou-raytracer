# include "../raytracer/struct.h"
# include "../raytracer/vector2.h"
# include "../raytracer/vector.h"
# include "../raytracer/draw.h"

int main(void)
{
  struct triangle tri;
  tri.a = create(0,0,0);
  tri.b = create(10,0,0);
  tri.c = create(0,10,0);

  struct ray r;
  r.ori = create(2,2,-5);
  r.dir = create(0,0,-1);

  float t;
  printf("--->> %d\n", in_triangle(r, tri, &t));
  return 0;
}
