#ifndef STRUCT_H
# define STRUCT_H

#include <stdio.h>

struct vector
{
  float x;
  float y;
  float z;
};

struct ray
{
  struct vector dir;
  struct vector ori;
};

struct triangle
{
  struct vector a;
  struct vector b;
  struct vector c;
  struct vector n_a;
  struct vector n_b;
  struct vector n_c;
  struct triangle *next;
};

struct camera
{
  int width;
  int height;
  struct vector pos;
  struct vector u;
  struct vector v;
  float fov;
};

enum light_type
{
  AMBIANT,
  POINT,
  DIRECTION
};

struct light
{
  enum light_type type;
  struct vector color;
  struct vector pos;
  struct light *next;
};

struct model
{
  // Objects Properties
  struct vector Ka; // ambient reflectivity
  struct vector Kd; // diffuse reflectivity
  struct vector Ks; // specular reflectivity
  float Ns; // specular exponent
  float Ni; // index of refraction
  float Nr; // reflection coefficient
  float d; // transparency

  // Triangle Linked List
  struct triangle *tri;

  // next
  struct model *next;
};


struct camera *init_cam(FILE *f_in);
struct light *init_light(FILE *f_in, int *over);
struct model *init_mod(FILE *f_in);

#endif /* !STRUCT_H */
