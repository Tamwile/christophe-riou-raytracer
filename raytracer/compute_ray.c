# include <stdlib.h>

# include "struct.h"
# include "vector.h"
# include "vector2.h"
# include "draw.h"
# include "compute_ray.h"

struct model *next_impact_mod(struct model *mod, struct ray r,
			      struct triangle **tri, struct vector *imp)
{
  struct model *near = NULL;
  float dist_min = 0;
  for (; mod; mod = mod->next)
  {
    struct triangle *tmp = mod->tri;
    for (; tmp; tmp = tmp->next)
    {
      float t = 0;
      if (!in_triangle(r, *tmp, &t))
	continue;
      if (t < 0)
	continue;

      struct vector impact = sum_v(prod_ext(r.dir, t), r.ori);
      float dist = distance(r.ori, impact);
      if (dist < 100*EPS)
	continue;
      if (dist < dist_min || !near)
      {
	dist_min = dist;
	*tri = tmp;
	*imp = impact;
	near = mod;
      }
    }
  }
  return near;
}

int obstacle(struct model *mod, struct ray r, struct triangle *tri)
{
  for (; mod; mod = mod->next)
  {
    struct triangle *tmp = mod->tri;
    for (; tmp; tmp = tmp->next)
    {
      if (tmp == tri)
	continue;
      
      float t = 0;
      if (!in_triangle(r, *tmp, &t))
	continue;
      if (t < 0)
	continue;
      return 1;
    }
  }
  return 0;
}

int in_sphere(struct light *lux, struct ray r)
{
  struct vector ori = r.ori;
  struct vector dir = r.dir;
  struct vector pos = lux->pos;
  float tmpA = ori.x - pos.x;
  float tmpB = ori.y - pos.y;
  float tmpC = ori.z - pos.z;

  float det_b = dir.x * tmpA + dir.y * tmpB + dir.z * tmpC;
  float det_a = dir.x * dir.x + dir.y * dir.y + dir.z * dir.z;
  float det_c = tmpA * tmpA + tmpB * tmpB + tmpC * tmpC - LUX_RADIUS*LUX_RADIUS;

  if (det_b * det_b - 4 * det_a * det_c >= 0)
    return 1;
  return 0;
}

struct light *next_impact_light(struct light *lux, struct ray r, float *dist)
{
  struct light *near = NULL;
  float dist_min = 0;
  for (; lux; lux = lux->next)
  {    
    if (lux->type != DIRECTION || !in_sphere(lux, r))
      continue;

    if (scalaire(vect_point(r.ori, lux->pos), r.dir) < 0)
      continue;
    
    *dist = distance(r.ori, lux->pos);
    if (*dist < dist_min || !near)
    {
      dist_min = *dist;
      near = lux;
    }
  }
  return near;
}

struct vector ambiant_color(struct light *lux)
{
  struct vector out =
  {
    .x = 0,
    .y = 0,
    .z = 0
  };

  for (; lux; lux = lux->next)
  {
    if (lux->type == AMBIANT)
      out = sum_v(out, lux->color);
    
  }
  return out;
}

/**
 * 1ère version
 * struct vector directio_color(struct light *lux, struct vector pos)
 * comment accéder à la normale ?
 */
struct vector directio_color(struct light *lux, struct vector pos,
			     struct triangle *tri, struct model *mdl)
{
  struct vector out =
  {
    .x = 0,
    .y = 0,
    .z = 0
  };

  for (; lux; lux = lux->next)
  {
    if (lux->type == DIRECTION)
    {
      struct ray r =
      {
	.ori = pos,
	.dir = prod_ext(lux->pos, -1)
      };

      if (obstacle(mdl, r, tri))
	continue;
      
      float Ld = -scalaire(vect_point(pos, lux->pos), tri->n_a);
      if (Ld < 0)
	Ld = 0;
      if (Ld > 1)
	Ld = 1;
      struct vector tmp = prod_ext(lux->color, Ld);
      out = sum_v(out, tmp);
    }
  }
  return out;
}

struct vector point_color(struct light *lux, struct vector pos,
			  struct triangle *tri, struct model *mdl)
{
  struct vector out =
  {
    .x = 0,
    .y = 0,
    .z = 0
  };

  for (; lux; lux = lux->next)
  {
    if (lux->type == POINT)
    {
      struct ray r =
      {
	.ori = pos,
	.dir = vect_point(pos, lux->pos)
      };

      if (obstacle(mdl, r, tri))
	continue;
      
      float Ld = -scalaire(vect_point(pos, lux->pos), tri->n_a);
      if (Ld < 0)
	Ld = 0;
      if (Ld > 1)
	Ld = 1;
      float dist = distance(pos, lux->pos);
      struct vector tmp = prod_ext(lux->color, Ld);
      tmp = prod_ext(lux->color, 1 / dist);
      out = sum_v(out, tmp);
    }
  }
  return out;
}

struct vector touch_mesh(struct model *m, struct light *lux, struct vector imp,
			 struct triangle *tri, struct model *full_mod)
{
  //printf("Ka:%f - %f - %f\n", (m->Ka).x, (m->Ka).y, (m->Ka).z);
  struct vector amb = mult_v(ambiant_color(lux), m->Ka);
  struct vector dir = mult_v(directio_color(lux, imp, tri, full_mod), m->Kd);
  struct vector poi = mult_v(point_color(lux, imp, tri, full_mod), m->Kd);
  struct vector over = sum_v(amb, dir); // sum_v => mult_v
  over = sum_v(over, poi);
  return modulo(over);
}

struct vector draw_ray(struct model *mod, struct light *lux, struct ray r)
{
  struct triangle *tri = NULL;
  struct vector imp;
  
  struct model *mdl = next_impact_mod(mod, r, &tri, &imp);
  struct vector color =
  {
    .x = 0,
    .y = 0,
    .z = 0
  };
  
  if (!mdl)
    return color;
  
  return touch_mesh(mdl, lux, imp, tri, mod);
}
