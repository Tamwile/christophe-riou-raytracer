# include <err.h>
# include <errno.h>
# include <stdlib.h>
# include <stdio.h>

# include "struct.h"
# include "parse.h"
# include "draw.h"
# include "raytracer.h"

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    errno = EINVAL;
    err(1, "usage: ./rt \"[input.svati]\" \"[ouput.ppm]\"\n");
  }
  FILE *f_in = fopen(argv[1], "r");
  if (!f_in)
    err(1, argv[1]);
  struct camera *cam = init_cam(f_in);
  int over = 0;
  struct light *lux = init_light(f_in, &over);

  struct model *mod = NULL;
  if (!over)
    mod = init_mod(f_in);
  fclose(f_in);
  
  FILE *f_out = fopen(argv[2], "w+");
  if (!f_out)
    err(1, argv[2]);
  draw_model(cam, lux, mod, f_out);
  fclose(f_out);

  free(cam);
  free_light(lux);
  free_model(mod);
  
  return 1;
}


