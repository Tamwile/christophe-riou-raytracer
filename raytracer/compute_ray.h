#ifndef COMPUTE_RAY_H
# define COMPUTE_RAY_H

# define LUX_RADIUS 0.02

struct vector draw_ray(struct model *mod, struct light *lux, struct ray r);

#endif /* !COMPUTE_RAY_H */
