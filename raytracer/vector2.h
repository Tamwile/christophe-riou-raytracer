#ifndef VECTOR2_H
# define VECTOR2_H

struct vector create(float a, float b, float c);
float scalaire(struct vector v1, struct vector v2);
float distance(struct vector v1, struct vector v2);
struct vector modulo(struct vector v);
struct vector mult_v(struct vector v1, struct vector v2);

#endif /* !VECTOR2_H */
