#ifndef PARSE_H
# define PARSE_H

struct light;
struct model;

void free_light(struct light *lux);
void free_model(struct model *m);
void free_all(char **tab);
char **parse_line(FILE *f_in);

#endif /* !PARSE_H */
