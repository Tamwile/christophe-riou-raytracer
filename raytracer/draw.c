# include <errno.h>
# include <math.h>
# include <stdio.h>

# include "struct.h"
# include "vector.h"
# include "vector2.h"
# include "compute_ray.h"
# include "draw.h"

static float det(struct vector v1, struct vector v2, struct vector v3)
{
  return v1.x * v2.y * v3.z + v1.y * v2.z * v3.x + v1.z * v2.x * v3.y
    - v1.z * v2.y * v3.x - v1.y * v2.x * v3.z - v1.x * v2.z * v3.y;
}

/**
 *     [x]
 * M * |y| = C
 *     [z]
 * with M and C known
 * return x, y, z in the vector out
 */
static struct vector cramer(struct vector v1, struct vector v2,
			    struct vector v3, struct vector cst)
{
  float m = det(v1, v2, v3);
  struct vector out = create(0, 0, 0);
  if (m < EPS && m > -EPS)
  {
    errno = EDOM;
    return out;
  }

  out.x = det(cst, v2, v3) / m;
  out.y = det(v1, cst, v3) / m;
  out.z = det(v1, v2, cst) / m;
  return out;
}

int in_triangle(struct ray r, struct triangle tr, float *t)
{
  struct vector ma = prod_ext(tr.a, -1.0f);
  struct vector res = cramer(prod_ext(r.dir, -1.0f), sum_v(tr.b, ma),
			     sum_v(tr.c, ma), sum_v(r.ori, ma));

  if (errno == EDOM)
  {
    errno = 0;
    return 0;
  }
  *t = res.x;
  
  if (0 > res.y || res.y > 1)
    return 0;
  if (0 > res.z || res.z > 1)
    return 0;
  if (res.y + res.z > 1)
    return 0;
  
  return 1;
}

static void init_output(FILE *f_out, int pix_x, int pix_y)
{
  fprintf(f_out, "P3\n");
  fprintf(f_out, "%d %d\n", pix_x, pix_y);
  fprintf(f_out, "255\n");
}

void draw_model(struct camera *cam, struct light *lux, struct model *obj,
		FILE *f_out)
{
  float pix_width = 1.0f;
  //float pix_height = 1.0f;
  struct vector u_nor = prod_ext(cam->u, 1 / norme(cam->u));
  struct vector v_nor = prod_ext(cam->v, 1 / norme(cam->v));
  struct vector w_nor = prod_ext(prod_v(u_nor, v_nor), -1);
  float l_dist = pix_width * cam->width / (2 * tanf((cam->fov*M_PI) / 360));
  struct vector center = sum_v(cam->pos, prod_ext(w_nor, l_dist));

  init_output(f_out, cam->width, cam->height);

  for (int h = -cam->height / 2; h < cam->height / 2; h++)
  {
    for (int w = -cam->width / 2; w < cam->width / 2; w++)
    {
      struct vector pixel = sum_v(prod_ext(u_nor, w), prod_ext(v_nor, h));
      pixel = sum_v(pixel, center);
      
      struct vector dir = vect_point(cam->pos, pixel);
      struct ray rayon =
      {
	.dir = dir,
	.ori = cam->pos,
      };
      struct vector color = draw_ray(obj, lux, rayon);
      int rgb_r = color.x * RANGE;
      int rgb_g = color.y * RANGE;
      int rgb_b = color.z * RANGE;
      fprintf(f_out, "%*d %*d %*d  ", DIGIT, rgb_r, DIGIT, rgb_g, DIGIT, rgb_b);
    }
    fprintf(f_out, "\n");
  }
}
