
# include "struct.h"
# include "vector.h"
# include "vector2.h"


/**
 * in  : 3 floats
 * out : 1 vector
 * create a vector from 3 floats
 */
struct vector create(float a, float b, float c)
{
  struct vector out =
  {
    .x = a,
    .y = b,
    .z = c
  };
  return out;
}


/**
 * in  : 2 vectors
 * out : 1 scalar
 * scalar product
 */
float scalaire(struct vector v1, struct vector v2)
{
  return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}


/**
 * in  : 2 vectors
 * out : 1 float
 * Distance between 2 points A and B 
 */
float distance(struct vector v1, struct vector v2)
{
  return norme(vect_point(v1, v2));
}


/**
 * in  : 1 vector
 * out : 1 vector
 * check that all values of v are less or equal to 1.0
 */
struct vector modulo(struct vector v)
{
  if (v.x > 1)
    v.x = 1.0f;
  if (v.y > 1)
    v.y = 1.0f;
  if (v.z > 1)
    v.z = 1.0f;
  return v;
}


/**
 * in  : 2 vectors
 * out : 1 vector
 * multiply 2 vectors coordinate per coordinate
 */
struct vector mult_v(struct vector v1, struct vector v2)
{
  struct vector out =
  {
    .x = v1.x * v2.x,
    .y = v1.y * v2.y,
    .z = v1.z * v2.z
  };
  return out;
}
