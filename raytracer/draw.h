#ifndef DRAW_H
# define DRAW_H

# define EPS 0.0001f
# define DIGIT 3
# define RANGE 255

int in_triangle(struct ray r, struct triangle tr, float *t);
void draw_model(struct camera *cam, struct light *lux, struct model *obj,
		FILE *f_out);

#endif /* !DRAW_H */
