#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "struct.h"
#include "parse.h"

void free_light(struct light *lux)
{
  while (lux)
  {
    struct light *tmp = lux;
    lux = lux->next;
    free(tmp);
  }
}

static void free_triangle(struct triangle *tri)
{
  while (tri)
  {
    struct triangle *tmp = tri->next;
    free(tri);
    tri = tmp;
  }
}

void free_model(struct model *m)
{
  while (m)
  {
    struct model *tmp = m->next;
    free_triangle(m->tri);
    free(m);
    m = tmp;
  }
}

static char *save_str(char *s)
{
  unsigned len = strlen(s);
  char *save = calloc(len + 1, 1);
  if (!save)
    err(1, "failed allocation");
  for (unsigned ite = 0; ite < len; ite++)
    *(save + ite) = s[ite];
  return save;
}

void free_all(char **tab)
{
  if (!tab)
    return;
  for(unsigned i = 0; *(tab+i) ; i++)
  {
    free(*(tab+i));
  }
  free(tab);
}


char **parse_line(FILE *f_in)
{
  char *buf = NULL;
  size_t size = 0;

  char **tab = calloc(1, sizeof(char*));
  if (getline(&buf, &size, f_in) == -1)
    return tab;
  
  const char s[2] = " ";
  char *token;

  token = strtok(buf, s);
  unsigned i = 0;

  for (; token != NULL; i++)
  {
    tab = realloc(tab, (i+2) * sizeof(char*));
    tab[i] = save_str(token);
    token = strtok(NULL, s);
  }
  tab[i] = NULL;
  return tab;
}
  


