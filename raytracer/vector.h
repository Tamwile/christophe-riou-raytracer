#ifndef VECTOR_H
# define VECTOR_H

struct vector sum_v(struct vector v1, struct vector v2);
struct vector prod_ext(struct vector v, float f);
struct vector prod_v(struct vector v1, struct vector v2);
float norme(struct vector v);
struct vector vect_point(struct vector v1, struct vector v2);

#endif /* !VECTOR_H */
