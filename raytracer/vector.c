# include <math.h>

# include "struct.h"
# include "vector.h"


/**
 * in  : 2 vectors
 * out : 1 vector
 * sum 2 vectors coordinate per coordinate
 */
struct vector sum_v(struct vector v1, struct vector v2)
{
  struct vector out =
  {
    .x = v1.x + v2.x,
    .y = v1.y + v2.y,
    .z = v1.z + v2.z
  };
  return out;
}


/**
 * in  : 1 vector, 1 float
 * out : 1 vector
 * do the extern muliplication between a vector and a float
 */
struct vector prod_ext(struct vector v, float f)
{
  struct vector out =
  {
    .x = v.x * f,
    .y = v.y * f,
    .z = v.z * f
  };
  return out;
}


/**
 * in  : 2 vectors
 * out : 1 vector
 * vectorial product
 */
struct vector prod_v(struct vector v1, struct vector v2)
{
  struct vector out =
  {
    .x = v1.y * v2.z - v1.z * v2.y,
    .y = v1.z * v2.x - v1.x * v2.z,
    .z = v1.x * v2.y - v1.y * v2.x
  };
  return out;
}


/**
 * in  : 1 vector
 * out : 1 float
 * Norm of the vector
 */
float norme(struct vector v)
{
  return sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);
}


/**
 * in  : 2 points (vectors)
 * out : 1 vector
 * return the vector with:
 * origin v1,
 * direction v1->v2,
 */
struct vector vect_point(struct vector v1, struct vector v2)
{
  struct vector out =
  {
    .x = v2.x - v1.x,
    .y = v2.y - v1.y,
    .z = v2.z - v1.z,
  };
  return out;
}
