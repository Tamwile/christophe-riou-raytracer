#include <err.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "vector2.h"
#include "parse.h"
#include "struct.h"

struct camera *init_cam(FILE *f_in)
{
  char **tab = NULL;
  do {
    free_all(tab);
    tab = parse_line(f_in);
  } while (strcmp(tab[0], "\n")  == 0);

  struct camera *cam = malloc(sizeof(struct camera));
  
  cam->width = atoi(tab[1]);
  cam->height = atoi(tab[2]);

  struct vector position;
  position.x = atof(tab[3]);
  position.y = atof(tab[4]);
  position.z = atof(tab[5]);
  cam->pos = position;

  struct vector vec_u;
  vec_u.x = atof(tab[6]);
  vec_u.y = atof(tab[7]);
  vec_u.z = atof(tab[8]);
  cam->u = vec_u;

  struct vector vec_v;
  vec_v.x = atof(tab[9]);
  vec_v.y = atof(tab[10]); 
  vec_v.z = atof(tab[11]);
  cam->v = vec_v;

  cam->fov = atof(tab[12]);
  free_all(tab);
  return cam;
}

static struct light *gen_light(char type, char **argc)
{
  struct light *l = malloc(sizeof (struct light));
  if (!l)
    err(1, "failed allocation");

  struct vector color;
  color.x = atof(argc[0]);
  color.y = atof(argc[1]);
  color.z = atof(argc[2]);
  l->color = color;
  l->next = NULL;
  
  if (type != 'a')
  {
    struct vector pos;
    pos.x = atof(argc[3]);
    pos.y = atof(argc[4]);
    pos.z = atof(argc[5]);
    l->pos = pos;
  }
  
  switch (type)
  {
  case 'a':
    l->type = AMBIANT;
    break;
  case 'd':
    l->type = DIRECTION;
    break;
  case 'p':
    l->type = POINT;
  }
  return l;
}

struct light *init_light(FILE *f_in, int *over)
{
  char **tab = NULL;

  struct light *out = NULL;
  
  do {
    do {
      free_all(tab);
      tab = parse_line(f_in);
    } while (!strcmp(tab[0], "\n"));
    if (strcmp(tab[0], "object") && tab[0])
    {
      if (!out)
	out = gen_light(tab[0][0], tab + 1);
      else
      {
	struct light *tmp = gen_light(tab[0][0], tab + 1);
	tmp->next = out;
	out = tmp;
      }
    }
  } while (strcmp(tab[0], "object") && tab[0]);
  *over = 0;
  if (!tab[0])
    *over = 1;
  free_all(tab);
  return out;
}

static struct vector gen_vect(char **tab)
{
  struct vector out;
  out.x = atof(tab[1]);
  out.y = atof(tab[2]);
  out.z = atof(tab[3]);
  return out;
}

static void gen_tri(FILE *f_in, struct model *m, char **tab)
{
  struct triangle *tri = malloc(sizeof (struct triangle));
  if (!tri)
    err(1, "failed allocation");
  
  for (unsigned ite = 0; ite < 3; ite++)
  {
    if (ite)
    {
      do {
	free_all(tab);
	tab = parse_line(f_in);
      } while (!strcmp(tab[0], "\n"));
    }

    switch (ite)
    {
    case 0:
      tri->a = gen_vect(tab);
      break;
    case 1:
      tri->b = gen_vect(tab);
      break;
    case 2:
      tri->c = gen_vect(tab);
    }
  }

  tri->next = m->tri;
  m->tri = tri;
  free_all(tab);
}

static void fill_tri(FILE *f_in, struct triangle *tri, char **tab, int nb)
{
  for (int ite = 0; ite < nb - 1; ite++)
    tri = tri->next;
  for (unsigned ite = 0; ite < 3; ite++)
  {
    if (ite)
    {
      do {
	free_all(tab);
	tab = parse_line(f_in);
      } while (!strcmp(tab[0], "\n"));
    }
    switch (ite)
    {
    case 0:
      tri->n_a = gen_vect(tab);
      break;
    case 1:
      tri->n_b = gen_vect(tab);
      break;
    case 2:
      tri->n_c = gen_vect(tab);
    }
  }
  free_all(tab);
}

static struct model *first_init()
{
  struct model *out = malloc(sizeof (struct model));
  if (!out)
    err(1, "failed allocation");
  out->tri = NULL;
  out->next = NULL;
  out->Ka = create(0, 0, 0);
  out->Kd = create(0, 0, 0);
  out->Ks = create(0, 0, 0);
  out->Ns = 0;
  out->Ni = 1;
  out->Nr = 0;
  out->d = 1;
  return out;
}

static int arg_type(char *arg)
{
  char test[][3] =
  {
    "Ka",
    "Kd",
    "Ks",
    "Ns",
    "Ni",
    "Nr",
    "d"
  };
  
  for (unsigned ite = 0; ite < 7; ite++)
  {
    if (!strcmp(test[ite], arg))
      return ite;
  }
  return -1;
}

static void set_arg(struct model *m, char **args)
{
  switch (arg_type(args[0]))
  {
  case 0:
    m->Ka = gen_vect(args);
    break;
  case 1:
    m->Kd = gen_vect(args);
    break;
  case 2:
    m->Ks = gen_vect(args);
    break;
  case 3:
    m->Ns = atof(args[1]);
    break;
  case 4:
    m->Ni = atof(args[1]);
    break;
  case 5:
    m->Nr = atof(args[1]);
    break;
  case 6:
    m->d = atof(args[1]);
  }
}

struct model *init_mod(FILE *f_in)
{
  char **tab = parse_line(f_in);
  
  struct model *out = first_init();
  int first = 1;
  int ite = 0;
  do {
    while (tab[0] && !strcmp(tab[0], "\n"))
    {
      free_all(tab);
      tab = parse_line(f_in);
    }
    if (tab[0] && (first || !strcmp(tab[0], "object")))
    {
      if (!first)
      {
	struct model *tmp = first_init();
	tmp->next = out;
	out = tmp;
      }
      do {
	while (tab[0] && (!strcmp(tab[0], "\n") || !strcmp(tab[0], "object")))
	{
	  free_all(tab);
	  tab = parse_line(f_in);
	}
	if (arg_type(tab[0]) != -1)
	{
	  set_arg(out, tab);
	  tab = parse_line(f_in);
	}
      } while (tab[0] && (arg_type(tab[0]) != -1 || !strcmp(tab[0], "\n")));
      ite = 0;
      first = 0;
    }
    if (tab[0])
    {
      if (strlen(tab[0]) == 1)
      {
	gen_tri(f_in, out, tab);
	ite++;
      }
      else
      {
	if (ite > 0)
	{
	  fill_tri(f_in, out->tri, tab, ite);
	  ite--;
	}
      }
      tab = parse_line(f_in);
    }
  } while (tab[0]);
  free_all(tab);
  return out;
}
